## 基本命令

    数据库四步orm (doctrine)
    安装
     composer require symfony/orm-pack
     composer require --dev symfony/maker-bundle
    
    .env 创建数据库(如果存在不需要创建)
    php bin/console doctrine:database:create

    第一步
    php bin/console make:entity 创建entity 实体,可以理解为模型model

    第二步生成迁移文件
    php bin/console make:migration 

    第三步同步数据库结构
    php bin/console doctrine:migrations:migrate

    第四步 生成 getter 和 setter 方法（如果手动修改结构的时候）
    php bin/console make:entity --regenerate App

```bash 
   ssh root@192.168.1.1:/var/www/
   sudo service nginx reload
   sudo service php-fpm reload
```