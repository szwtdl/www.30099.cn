<?php


namespace App\Controller\Admin;


use App\Controller\BaseController;
use App\Entity\User;
use App\Form\UserType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Psr\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
 * @Route("/admin")
 */
class AdminController extends BaseController
{

    /**
     * @Route("/list",name="admin_list",methods="GET|POST")
     */
    public function index(Request $request)
    {
        return $this->render('admin/index.html.twig', [
            'username' => 'xxxx'
        ]);
    }


    /**
     * @Route("/create",name="admin_create",methods="GET|POST")
     */
    public function create(Request $request): Response
    {
        $user = $this->getUser();
        $form = $this->createForm(UserType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
//            $eventDispatcher->dispatch(new CommentCreatedEvent($comment));
            return $this->redirectToRoute('admin_list');
        }
        return $this->render('admin/user/create_form.html.twig', [
            'form' => $form->createView(),
        ]);
    }


}