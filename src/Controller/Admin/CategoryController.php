<?php


namespace App\Controller\Admin;


use App\Controller\BaseController;
use App\Entity\Category;
use App\Form\CategoryType;
use App\Repository\CategoryRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/category")
 * Class CategoryController
 * @package App\Controller\Admin
 */
class CategoryController extends BaseController
{

    /**
     * @Route("/list-{limit}-{page}",name="admin_category_list",methods="GET|POST",defaults={"page": "1","limit":20})
     */
    public function index(CategoryRepository $category,$page,$limit)
    {
        return $this->render('admin/category/index.html.twig', [
            'paginator' => $category->getList($page, $limit),
        ]);
    }
    /**
     * @Route("/create",name="admin_category_create",methods="GET|POST")
     */
    public function create(Request $request): Response
    {
        $category = new Category();
        $form = $this->createForm(CategoryType::class,$category);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($category);
            $em->flush();
//            $eventDispatcher->dispatch(new CommentCreatedEvent($comment));
            return $this->redirectToRoute('admin_category_list');
        }
        return $this->render('admin/category/create_form.html.twig', [
            'form' => $form->createView(),
        ]);
    }

}