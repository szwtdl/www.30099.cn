<?php


namespace App\Controller\Admin;


use App\Controller\BaseController;
use App\Entity\Category;
use App\Entity\Product;
use App\Form\ProductType;
use App\Repository\ProductRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("admin/product")
 * Class ProductController
 * @package App\Controller\Admin
 */
class ProductController extends BaseController
{
    /**
     * @Route("/list-{limit}-{page}",name="admin_product_list",methods="GET|POST",defaults={"page": "1","limit":20})
     */
    public function index(ProductRepository $productRepository, $page, $limit)
    {
        return $this->render('default/product/index.html.twig', [
            'paginator' => $productRepository->getList($page, 1),
        ]);
    }

    /**
     * @Route("/create",name="admin_product_create",methods="GET|POST")
     */
    public function create(Request $request)
    {
        $product = new Product();
        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            dd($form->getData());
            $em = $this->getDoctrine()->getManager();
            $em->persist($product);
            $em->flush();
//            $eventDispatcher->dispatch(new CommentCreatedEvent($comment));
            return $this->redirectToRoute('admin_category_list');
        }
        return $this->render('admin/category/create_form.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}