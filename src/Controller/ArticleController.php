<?php


namespace App\Controller;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/article")
 */
class ArticleController extends BaseController
{

    /**
     * @Route("/list-{type}",
     *     methods="GET|POST",
     *     name="article_list",
     *     requirements={
     *     "type"="\d+"
     *     })
     */
    public function index(Request $request, int $type)
    {
        return $this->render('default/article/index.html.twig', [
            'username' => 'pengjian'
        ]);
    }

    /**
     * @Route("/detail-{id}",
     *     methods="GET|POST",
     *     name="article_detail",
     *     requirements={"id"="\d+"}
     *     )
     */
    public function detail(Request $request, int $type)
    {
        return $this->render('article/detail.html.twig');
    }

}