<?php


namespace App\Controller;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/case",methods="GET")
 */
class CaseController extends BaseController
{

    /**
     * @Route("/list-{type}",name="case_list",requirements={"type"="\d+"})
     */
    public function index(Request $request, int $type)
    {

    }

    /**
     * @Route("/detail-{id}",name="case_detail",format="html",requirements={"id"="\d+"})
     */
    public function detail(Request $request, int $id)
    {

    }

}