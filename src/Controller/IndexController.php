<?php


namespace App\Controller;


use App\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class IndexController extends BaseController
{

    /**
     * @Route("/",methods="GET|POST",name="welcome")
     */
    public function index(Request $request,HttpClientInterface $client)
    {
//        $response = $client->request('GET',"http://opencart.szwtdl.cn/index.php?route=product/category&path=20");
//        $crawler = new Crawler($response->getContent());
//        $data = $crawler->filterXPath('//*[@id="content"]/div[4]/div')->each(function (Crawler $node, $i) {
//            $list[$i] = [
//                'photo' => $node->filterXPath('//div/div[1]/a/img/@src')->text(),
//                'title' => $node->filterXPath('//div/div[2]/div[1]/h4/a')->html(),
//                'desc' => $node->filterXPath('//div/div[2]/div[1]/p')->html()
//            ];
////            $list[]['shop_price'] = $node->filterXPath('//div/div[2]/div[1]/p[2]/span[@class="price-new"]')->html();
////            $list[]['market_price'] = $node->filterXPath('//div/div[2]/div[1]/p[2]/span[@class="price-old"]')->html();
////            $list[]['member_price'] = $node->filterXPath('//div/div[2]/div[1]/p[2]/span[@class="price-tax"]')->html();
//            return $list;
//        });
//        dd($data);
//        exit();
        return $this->render('default/index.html.twig', [
            'username' => 'welcome nginx'
        ]);
    }

    /**
     * @Route("/create",methods="GET|POST",name="user_create")
     */
    public function create(Request $request,UserPasswordHasherInterface $passwordHasher,ValidatorInterface $validator)
    {
        $user = new User();
        $user->setNickname('彭剑1');
        $user->setUsername('szwtdl');
        $user->setPassword($passwordHasher->hashPassword($user,'12345678'));
        $user->setEmail('pengjian@gmail.com');
        $user->setRoles(['ROLE_ADMIN','ROLE_USER']);
        $user->setRegIp($request->getClientIp());
        $user->setLastIp($request->getClientIp());
        $user->setIsDeny(0);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($user);
        $entityManager->flush();
        dd($user->getSlug());
    }
}