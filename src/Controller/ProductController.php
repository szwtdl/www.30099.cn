<?php


namespace App\Controller;


use App\Entity\Category;
use App\Entity\Product;
use App\Repository\ProductRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @Route("/product",methods="GET")
 */
class ProductController extends BaseController
{
    /**
     * @Route("/list-{type}-{page}",name="product_list",defaults={"page": "1", "_format"="html"},requirements={"type"="\d+"})
     */
    public function index(Request $request, int $type, int $page, ProductRepository $products, ValidatorInterface $validator)
    {
        $category = new Category();
        $category->setName('公司新闻');
        $category->setTitle('公司新闻SEO标题');
        $category->setKeywords('SEO关键词');
        $category->setDescription("SEO描述");
        $category->setSortNum(99);
        $category->setIsDeny(0);

        $product = new Product();
        $product->setName('小大师法是否');
        $product->setTitle('最新系统升级');
        $product->setKeywords('xxxSEO');
        $product->setDescription('Ergonomic and stylish!');
        $product->setContent('我是消息内容');
        $product->setPhoto('system/demo.jpg');
        $product->setImages('system/demo.jpg');
        $product->setSortNum(99);
        $product->setIsDeny(0);
        $product->setIsDelete(0);
        $product->setCategory($category);
        $errors = $validator->validate($product);
        if (count($errors) > 0) {
            $errorsString = (string) $errors;
            return new Response($errorsString);
        }
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($category);
        $entityManager->persist($product);
        $entityManager->flush();
        return $this->render('default/product/index.html.twig', [
            'paginator' => $products->getList($page, $type),
        ]);
    }

    /**
     * @Route("/detail-{id}",name="product_detail",requirements={"id"="\d+"})
     */
    public function detail(int $id,ProductRepository $productRepository)
    {
        return $this->render('default/product/detail.html.twig',[
            'item' => $productRepository->find($id)
        ]);
    }
}