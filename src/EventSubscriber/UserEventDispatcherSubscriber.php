<?php

namespace App\EventSubscriber;

use Monolog\Logger;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\Event\AuthenticationEvent;

class UserEventDispatcherSubscriber implements EventSubscriberInterface
{
    public function onSecurityAuthenticationSuccess(AuthenticationEvent $event)
    {
        $log = new Logger('user');
        $log->info("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",[
            'event' => $event->getAuthenticationToken()
        ]);
    }

    public static function getSubscribedEvents()
    {
        return [
            'security.authentication.success' => 'onSecurityAuthenticationSuccess',
        ];
    }
}
