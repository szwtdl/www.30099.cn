<?php

namespace App\Form;

use App\Entity\Category;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CategoryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'label.name',
                'required' => true
            ])
            ->add('title', TextType::class, [
                'label' => 'label.title',
            ])
            ->add('keywords', TextType::class, [
                'label' => 'label.keywords',
            ])
            ->add('description', TextType::class, [
                'label' => 'label.description',
            ])
            ->add('sort_num', NumberType::class, [
                'label' => 'label.sort_num',
                'required' => false,
                'empty_data' => 199
            ])
            ->add('is_deny', ChoiceType::class, [
                'label' => 'label.is_deny',
                'choices' => array(
                    'label.yes' => 1,
                    'label.no' => 2,
                ),
                'expanded' => true
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Category::class,
        ]);
    }
}
