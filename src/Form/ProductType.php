<?php

namespace App\Form;

use App\Entity\Category;
use App\Entity\Product;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('title')
            ->add('keywords')
            ->add('description')
            ->add('images')
            ->add('photo')
            ->add('content')
            ->add('sort_num',NumberType::class,[

                'empty_data' => 99
            ])
            ->add('is_deny',ChoiceType::class,[
                'choices' => array(
                    'label.yes'=> 0,
                    'label.no' => 1,
                ),
                'expanded' => true
            ])
            ->add('is_delete',ChoiceType::class,[
                'choices' => array(
                    'label.yes'=> 0,
                    'label.no' => 1,
                ),
                'expanded' => true
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Product::class,
        ]);
    }
}
