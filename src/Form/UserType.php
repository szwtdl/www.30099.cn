<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RadioType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', TextType::class, [
                'label' => 'label.username',
                'required' => true
            ])
            ->add('nickname', TextType::class, [
                'label' => 'label.nickname',
                'required' => true
            ])
            ->add('password', PasswordType::class, [
                'label' => 'label.password',
                'required' => true
            ])
            ->add('email', EmailType::class, [
                'label' => 'label.email',
            ])
            ->add('roles', CheckboxType::class, [
                'label' => 'label.roles',
                'required' => true
            ])
            ->add('is_deny', RadioType::class, [
                'label' => 'label.is_deny',
                'required' => true,
                'data' => false
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
